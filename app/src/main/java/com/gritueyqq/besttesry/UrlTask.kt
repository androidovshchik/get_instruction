package com.gritueyqq.besttesry

import android.os.AsyncTask
import timber.log.Timber
import java.net.HttpURLConnection
import java.net.URL

open class UrlTask : AsyncTask<Void, Void, String?>() {

    override fun doInBackground(vararg voids: Void): String? {
        return try {
            val url = URL("http://rutafagre.ru/offroad")
            val urlConnection = url.openConnection() as HttpURLConnection
            return urlConnection.inputStream.use {
                it.bufferedReader().readText()
            }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}
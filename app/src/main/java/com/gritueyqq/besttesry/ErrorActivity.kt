package com.gritueyqq.besttesry

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.PointF
import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class ErrorActivity : AppCompatActivity() {

    @SuppressLint("BinaryOperationInTimber")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        main.isZoomEnabled = false
        main.setOnTouchListener { _, event ->
            if (event.action != MotionEvent.ACTION_DOWN) {
                return@setOnTouchListener false
            }
            val transformCoordTouchToBitmap = main.javaClass.getDeclaredMethod(
                "transformCoordTouchToBitmap",
                Float::class.java, Float::class.java, Boolean::class.java
            )
            transformCoordTouchToBitmap.isAccessible = true
            val bitmapPoint = transformCoordTouchToBitmap.invoke(main, event.x, event.y, true) as PointF
            Timber.d(bitmapPoint.toString())
            if (bitmapPoint.x in 289.0..792.0) {
                if (bitmapPoint.y in 797.0..915.0) {
                    startActivity(Intent(applicationContext, WebActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    })
                } else if (bitmapPoint.y in 1233.0..1351.0) {
                    startActivity(Intent(applicationContext, InstructionActivity::class.java))
                }
            }
            return@setOnTouchListener false
        }
    }
}

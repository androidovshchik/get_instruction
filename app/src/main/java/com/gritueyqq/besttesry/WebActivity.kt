@file:Suppress("DEPRECATION")

package com.gritueyqq.besttesry

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import timber.log.Timber

class WebActivity : AppCompatActivity() {

    private lateinit var webView: WebView

    @SuppressLint("SetJavaScriptEnabled", "SdCardPath")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val container = FrameLayout(applicationContext)
        container.layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT
        )
        val progressBar = ProgressBar(applicationContext)
        progressBar.layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        )
        progressBar.indeterminateDrawable.setColorFilter(
            ContextCompat.getColor(
                applicationContext,
                R.color.colorAccent
            ), PorterDuff.Mode.SRC_IN
        )
        progressBar.isIndeterminate = true
        webView = WebView(this)
        webView.layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT
        )
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.setSupportZoom(true)
        webView.settings.javaScriptEnabled = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.setSupportMultipleWindows(true)
        webView.settings.domStorageEnabled = true
        webView.settings.databaseEnabled = true
        if (Build.VERSION.SDK_INT < 19) {
            webView.settings.databasePath = "/data/data/$packageName/databases/"
        }
        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return false
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String?, failingUrl: String?) {
                super.onReceivedError(view, errorCode, description, failingUrl)
                if (progressBar.visibility != View.GONE) {
                    progressBar.visibility = View.GONE
                }
            }

            override fun onPageFinished(view: WebView, url: String) {
                CookieSyncManager.getInstance().sync()
                if (progressBar.visibility != View.GONE) {
                    progressBar.visibility = View.GONE
                }
            }
        }
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        } else {
            CookieManager.getInstance().setAcceptCookie(true)
        }
        CookieManager.getInstance().acceptCookie()
        container.addView(webView)
        container.addView(progressBar)
        setContentView(container)
        scheduleTask()
    }

    @SuppressLint("StaticFieldLeak")
    private fun scheduleTask() {
        object : UrlTask() {

            override fun onPostExecute(result: String?) {
                Timber.d("result $result")
                if (!isFinishing) {
                    if (TextUtils.isEmpty(result)) {
                        startActivity(Intent(applicationContext, ErrorActivity::class.java))
                        finish()
                    } else {
                        webView.loadUrl(result)
                    }
                }
            }
        }.execute()
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}

package com.gritueyqq.besttesry

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        progress.indeterminateDrawable.setColorFilter(
            ContextCompat.getColor(
                applicationContext,
                android.R.color.white
            ), PorterDuff.Mode.SRC_IN
        )
        progress.isIndeterminate = true
        Handler().postDelayed({
            startActivity(Intent(applicationContext, WebActivity::class.java))
            finish()
        }, 1500)
    }

    override fun onBackPressed() {}
}
